﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RoleBasedAuthentication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize()]
        public ActionResult AuthenticatedOnly()
        {
            return Content("You are logged in!");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AdminOnly()
        {
            return Content("You are an admin!");
        }
    }
}