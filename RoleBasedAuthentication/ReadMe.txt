﻿Step 1:
Create a Web Application with Individual User Accounts for Authentication and Authorisation.
This sets up the project with ASP Identity.
Remember that the connection string defined in your web.config will be used to identify which
database server and database file to use.

Step 2:
Go to Startup.cs
In this file, add a new method to create the users and roles.
This method must be executed AFTER the ConfigureAuth method, which is used to setup OWIN (Open Web Interface for .Net)
The new method will access your IdentityDBContext found in the IdentityModels class.  Unless you have changed it,
the specific context to use is the ApplicationDbContext.
Follow the code and in-line comments to see how:
	1) the Role was created;
	2) the User was added; and
	3) the User was given a Role

When creating the new user, make sure that the username and email match.
Make also sure that the password meets all the password policy requirements.
Check that the user was created successfully!

Step 3:
Create actions that use the [Authorize()] attribute filter.
Only logged in users can access these actions.

Create actions that use the [Authorize(Roles = "****")] attribute filter.
Only users that are logged in and have one of the required roles in the attribute can access these actions.

Two sample actions have been created in the HomeController.

